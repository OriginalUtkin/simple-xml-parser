
<?php
require("global_variables.php");

/*
 * Funkce provadi kontrolu argumentu ze vstupu (jejich spravnost a pocet)
 * 
 * @return void
*/
function params_test(){

	global $errors, $argv, $longopts, $shortopts;

	$argv_test = $argv;
	array_shift($argv_test);
	foreach ($argv_test as $key) {
                $countAcc = 0;
           $countEq  = substr_count($key, "=");
                if($countEq == 0){
                    $countAcc = substr_count($key, "-");
                }else{
                    $position = strpos($key, "=");
                    $keyTest = substr($key, 0,$position);
                    $countAcc = substr_count($keyTest, "-");
                }
		
		//"-" symbol vyskytuje v argumentu pouze jedenkrat
		if($countAcc == 1){
			$value_test = substr($key, 1);
			if($value_test === $shortopts){
				continue;
			}else //pokud zadany argument obsahuje pouze jeden symbol "-" a neni argumentem "-n" -> chyba
				error_exit($errors["WRONG_P"],1);
		}else if($countAcc == 2){
			//"=" symbol vyskytuje v argumentu pouze jedenkrat
			if($countEq > 0){
                            $position = strpos($key, "=");
                            $keyTest = substr($key, 2,$position-2);
				// "--INPUT_ARGUMENT=" nemuze byt argumentem "--help"
				if($keyTest === "help") // Pokud vstupni argument se rovna "--help=" -> chyba
					error_exit($errors["WRONG_P"],1);
				if(in_array($keyTest.":", $longopts)){ //jinak je to spravny argument z longopts pole, ktery musi obsahovat nejakou hodnotu
					continue;
				}else
					error_exit($errors["WRONG_P"],1);
			//Vstupni argument neobsahuje symbol "="
			}else if($countEq == 0){
				$value_test = substr($key, 2);
				if(in_array($value_test, $longopts)){
					continue;
				}else
					error_exit($errors["WRONG_P"],1);
			}else
				error_exit($errors["WRONG_P"],1);
		}else
			error_exit($errors["WRONG_P"],1);
	}
}


/*
 * Funkce nastavi globalne promenny z global_variables.php
 * 
 * @return void
*/
function params(){
	//All global variables from 'global_variables.php'
	global $parameters,$errors,$qf, $final_query, $n,$outputXMLName,$inputXMLName,$n,$root;
	//Work with all input arguments in cykle
	if(count($parameters) > 0){
		foreach ($parameters as $argName => $argValue){
			//'--help' argument
			//echo $argName. " ".$argValue."\n";
			if($argName === "help"){
				if(count($parameters) == 1){
					print_help();
				}else{
					error_exit($errors["HELP_COMBINED"],1);
				}
			}
			//'--input = <input_file>' argument
			if($argName === "input"){
				if(isset($argValue) && check_input($argValue))
					$inputXMLName = $argValue;
                                else
					error_exit($errors["INPUT_ERROR"],2);
			}
			//'--output = <output_file>' argument
			if($argName === "output"){
				if(isset($argValue))
					$outputXMLName = $argValue;
				check_output($outputXMLName);
			}
			//'--query = <query_string>' query argument
			if($argName === "query"){
				if(isset($argValue)){
						$query = $argValue;
						$final_query = $query;
				}
			}
			//'--qf = <qf_input_file>' qf argument
			if($argName === "qf"){
				if(isset($argValue) && check_input($argValue))
					$qf = $argValue;
				else
					error_exit($errors["QF_ERROR"],2);
                                
				$qf_file = fopen($qf, "r");
				$final_query = file_get_contents($qf);
				fclose($qf_file);
			}
			//'-n' XML-head generate argument
			if($argName === "n")
				$n = TRUE;

			//'--root=<string>' argument.Generovani root pro vysledny document
                        if($argName === "root" && isset($argValue)){
                            $root = $argValue;
                        }
		}

	}else{ //Neocekavany vstupni argument -> chyba
		error_exit($errors["NO_P"],1);
	}
        if(isset($outputXMLName)){ //Vysledek musi byt zapsan do souboru
            prepareFile();
        }
        
	if(isset($qf) && isset($query))
		error_exit($errors["DOUBLE_QUERY"],1);
	if(!isset($qf) && !isset($query))
		error_exit($errors["EMPTY_QUERY"],1);
}


/*
 * Funkce provadi kontrolu vstupniho souboru
 * 
 * @return TRUE  - pokud zadany soubor existuje a mozne precist
 * @return FALSE - soubor neexistuje nebo nelze cist obsah souboru
 */
function check_input($inputFile){

	if(is_readable($inputFile))
		return TRUE;
	else
		return FALSE;
}


/*
 * Funkce prepravuje vysledny soubor:
 * 1)Vytvoreni hlavicky souboru ve spravne podobe
 * 2)Vytvoreni pomocneho tag-elementu bez ktereho nejde pouzit zakladni funkce pro praci
 *   s SimpleXMLDocument elementem. POmocny tag-element bude smazan na konci scriptu
 * 
 * @return void
 */
function prepareFile(){
    global $n,$root,$outputXMLName;
    
    $list = file($outputXMLName);
    
    //vysledny soubor musi obsahovat hlavicku
    if(!$n){
        //pokud vysledny soubor je prazdny
        if(empty($list[0])){
            $list[0] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            //node ktery pak bude smazan
            $list[1] = "<topkekcheburek/>";
            file_put_contents($outputXMLName, implode('', $list));
            //Kontrola ze byl zadan argument --root
            if(isset($root)){
                $list[1] = "<".$root."/>";
                file_put_contents($outputXMLName, implode('', $list));
            }
        }                  
    }else{ //Vysledny soubor nemusi obsahovat hlavicku
         $list[0] = "<topkekcheburek/>\n";
         file_put_contents($outputXMLName, implode('', $list));
         
         if(isset($root)){
             $list[0] = "<".$root."/>";
             file_put_contents($outputXMLName, implode('', $list));
         }
    }
}


/*
 * Funkce otevira soubor z argumentu --output pro zapis
 * V pripade ze soubor neexistuje provadi vytvoreni souboru 
 * 
 * @params String $outputFile - jmeno souboru z argumentu --output
 * @return void
 */
function check_output($outputFile){
    
    $file = fopen($outputFile, "w+");
}


/*
 * Funkce vytiskne napovedu po zadani argumentu --help
 * 
 * @return void
 */
function print_help(){
    $helpMessage = "--input =filename   - Input file with xml\n" .
			"--output = filename  - Output file with xml\n" .
			"--query = 'query'        - Query under xml - can not be used with -qf attribute\n" .
			"--qf = filename.ext      - Filename with query under xml\n" .
			"-n                     - Xml will be generated without XML header\n" .
			"-r = element             - Name of root element\n";
    fwrite(STDOUT,$helpMessage);
    exit(0);
}


/*
 * Funkce vypise chybu pri provadeni scriptu do STDERR a ukonci script s nastavenim
 * kodu chyby
 * 
 * @params $error - polozka pole z global_variables, ktera obsahuje text chyby
 * @params $code - kod chyby se kterym script ukonci svou praci
 * 
 * @return void
 */
function error_exit($error,$code){
    fwrite(STDERR, $error);
    exit($code);
}

?>
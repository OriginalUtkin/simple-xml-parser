<?php
/*
 * Funkce provadi rozbor dotazu ze vstupniho argumentu --qf nebo --query na zaklade regularnich vyrazu . V pripade chyby
 * Vypise popis chyby a ukonci script s navratovym kodem  cislo 80
 * Funkce obsahuje zpracovani :
 * 1)Obecny format dotazu. V pripade ze dotaz neobsahuje zakladni klicova slova jako SELECT a FROM -> chyba 
 * 2)Zpracovani sekce SELECT
 * 3)Zpracovani sekce FROM
 * 4)Zpracovani sekce WHERE (vc. zpracovani klic.slova NOT)
 * 5)Zpracovani LIMIT
 * 
 * @return void
*/
    function parserQuery(){
        
	require_once("global_variables.php");
        require_once ("arguments.php");
	global $final_query,$errors,$selectGlobal, $fromGlobal, $limitGlobal, $notStatement, $elemWhereGlobal, $literalStr, $literalNum,$relationOp;
        
        //Zpracovani obecheno formatu dotazu
	$order = "/(?<=SELECT\s)\s*(\w+)\s*FROM/";

	$query = $final_query;

	if(!preg_match($order, $query)){ //Pokud vstupni dotaz neobsahuje klicova slova SELECT a FROM, nebo neobsahuje element ze SELECT
            error_exit($errors["Q_STANDART_FORMAT"], 80); // Ukonceni scriptu
	}
		
	else{ //Jinak provadime zpracovani jednotlyvych casti dotazu

	//---SELECT SECTION---
	$select_reg = "/(?<=SELECT\s)\s*(\w+)/";
	preg_match($select_reg, $query,$selectArray);
	//Posuv o jeden element
	array_shift($selectArray);
	//Prevadime polozku pole do String a ukladam go globalne promenne
	$selectGlobal = implode($selectArray);

	//---FROM SECTION---
	$from_reg = "/(?<=FROM\s)\s*(\w*\.*\w*)/i";
	preg_match($from_reg, $query, $fromArray);
	array_shift($fromArray);
	$fromString = implode($fromArray);	
	$fromGlobal = explode('.', $fromString);
        if(($fromGlobal[0] === "WHERE" || $fromGlobal[0] === "LIMIT")){$fromGlobal[0] = "";}

	//---LIMIT SECTION---
	//Pokud query obsahuje klicove slovo LIMIT potrebujeme zjistit hodnotu, ktera je argumentem
	$limit_search_reg = "/.*(LIMIT).*/";
	//Query obsahuje klicove slovo LIMIT. Je nutne provest kontrolu parametru u LIMIT
	if(preg_match($limit_search_reg, $query)){
		//dostavame vsecno co mame po LIMIT
		$limit_reg = "/(?<=LIMIT\s)\s*([a-zA-Z0-9.]+)/";
		preg_match($limit_reg,$query, $limitArray);
		array_shift($limitArray);
		$sectionLimit = trim(implode($limitArray));		
		if((is_numeric($sectionLimit)) && (preg_match("/([^0-9]+)/", $sectionLimit) == 0)){
			$limitGlobal = $sectionLimit;
		}	
                else{error_exit ($errors["WRONG_LIMIT"], 80);}//Wrong limit value      
	}

	//---WHERE SECTION---
	if(preg_match("/.*\s+(WHERE).*/", $query)){
	
		$where_search_reg = preg_match("/(?<=WHERE)(\s+NOT+)*\s+(\w*\.*\w*)\s*(CONTAINS|\<|\>|\=|\!\=)\s*(\"\w+\"|\w+|\(\w+\)|[+,-]?\d+|\"\d\")\s*/", $query,$where_array);
		//return here code 80
		if(!$where_search_reg){
                    error_exit($errors["WHERE_FORMAT"], 80);
		}

		preg_match("/(\s*NOT\s)+/", $query,$counter);
                //Pocet klicovych slov NOT 
		if(!empty($counter)){
			$notCounter = substr_count($counter[0], "NOT");
		}else
			$notCounter = 0;
	
		//Pocet NOT je sudy !(!(a=b)) == (a=b)
		if(($notCounter % 2) == 0){
			$notStatement = FALSE;
		}
		//Pocet NOT je lichy !(a=b) == (a != b)
		else{
			$notStatement = TRUE;
		}
	
		unset($where_array[1]);
		$where_array = array_values($where_array);
		//Ulozeni <ELEMENT-OR-ATTRIBUTE> do pole
		$elemWhereGlobal = explode('.', $where_array[1]);
		//Spracovani <REALATION-OPERATOR>. Pokud nastaven NOT - inverze operatoru, jinak ulozime do globalne promenne
		if($notStatement){
			if($where_array[2] === "=")
				$relationOp = "!=";
			if($where_array[2]=== ">")
				$relationOp = "<";
			if($where_array[2] === "<")
				$relationOp = ">";
			if($where_array[2] === "CONTAINS")
				$relationOp = "NOT CONTAINS";
		}else
			$relationOp = $where_array[2];
		//Zpracovani sekce <LITERAL>
		if(is_numeric($where_array[3])){
                    $literalNum = $where_array[3];
                }
			
		else
			$literalStr = $where_array[3];
		//Pokud zadan <RELATION-OPERATOR> CONTAINS nebo NOT CONTAINS, sekce <LITERAL> musi obsahovat cislo ELSE return here code 80
		if(($relationOp === "CONTAINS" || $relationOp === "NOT CONTAINS") && isset($literalNum))
                    error_exit ($errors["CONTAINS_ERROR"], 80);
	}
    }
}
	
?>
<?php
	require_once("global_variables.php");
	require_once("arguments.php");
        require_once ("query_parse.php");
        require_once ("XMLwork.php");
       
        global $STDIN,$STDOUT,$inputXML,$outputXML,$final_query;

	/*Kontrola spravnosti vstupnich argumentu*/ 
	params_test();
        
  	/*Provadi ulozeni hodnot parametru do globalnich promennych z global_variables.php souboru. V pripade
	Neocekavaneho chovani vypise chybu a ukonci script */
	params();
        /*Provadi zpracovani QUERY z souboru nebo ze stdin. V pripade chybneho dotazu, vypise na stderr gde je chyba a ukonci script*/
        parserQuery();
  
        /*Prace s XML souborem a vyhledavani uzlu podle dotazu*/
        XMLwork();
        
        exit(0);
?>
<?php
	//Popis kratkych argumentu ze vstupu scriptu
	 $shortopts = "n";
	//Popis argumentu,zacinajicich "--"
	$longopts  = [
		"help",
		"input:",
		"output:",
		"query:",
		"qf:",
		"root:"
	];

	//Pole s popisem chyb ve tvaru [ERROR_NAME] => [ERROR_DESCRIPTION]
	$errors = [
    	"WRONG_P" => "ERROR : Some parameter is wrong. Use --help for print usage message\n",
    	"NO_P"	  => "ERROR : No parameters from input. Use --help for print usage message\n",
    	"HELP_COMBINED" => "ERROR : --help argument can't combined with other argument's \n",
    	"DOUBLE_QUERY"=>"ERROR : Can't use 'qf' and 'query' parametr together\n",
    	"EMPTY_QUERY"=>"ERROR : query not defined\n",
    	"INPUT_ERROR"=>"ERROR : Can't read input XML file or file not exist\n",
    	"QF_ERROR"=>"ERROR : Can't read input query file or file not exist\n",
        "Q_STANDART_FORMAT"=>"ERROR : Wrong query format. Format is : SELECT <Element> FROM <Element-Attribute>\n",
        "WRONG_LIMIT"=>"ERROR : Wrong limit format. Use INT value for limit\n",
        "WHERE_FORMAT"=>"ERROR : Wrong WHERE statement\n",
        "CONTAINS_ERROR"=>"ERROR : Number value can't be after \"CONTAINS\" or \"NOT CONTAINS\"\n",
        "NOT_INPUT"=>"ERROR : Input file not exist\n",
        "NOT_OUTPUT"=>"ERROR : Output file not exist\n",
        "WRONG_FILE_FORMAT"=>"EROR : Wrong format input file\n"
    ];

    $parameters = getopt($shortopts,$longopts);
    
    $STDOUT = fopen('php://stdout', 'w+');
    $STDIN = fopen('php://stdin', 'r');
    //Vstupni XML soubor.
    $inputXML;
    $inputXMLName;
    //Vystupni XML soubor
    $outputXML;
    $outputXMLName;
    //Vstupni dotaz z stdin
    $input_query;
    //Nacteny dotaz z souboru
    $qf;
    //Promenna obsahuje dotaz nacteny ze STDIN nebo z QF souboru.
    $final_query;
    //Promena obsahuje element, obalujici vysledky
    $root;
    //Priznak generovani XML headeru
    $n = FALSE;
    
    //Variables after query parse
    //SELECT variables
    $selectGlobal;
    //FROM variables : [0] - element ; [1] - attribute
    $fromGlobal;
 
    //WHERE variables
    $notStatement;
    $elemWhereGlobal; //[0] - element ; [1] - attribut
    $relationOp;
    $literalStr;
    $literalNum;
    //LIMIT variables
    $limitGlobal;
    $counterLimit = 0;
    
    //Counter pro break z cyklu foreach ve XMLWithQuery(test.php now)
    $counterForBreak = 0;
    //Pokud narazen prvni vyskyt elementu z FROM , tak dalsi spracovat nemusim(coz podle me neni spravne)
    $fromCounter = 0;
?>
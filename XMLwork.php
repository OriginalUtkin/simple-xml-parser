<?php
/*
 * Funkce provadi vyhledavani zadanych elementu ve vstupnim XML souboru
 * a pripravuje vystupni soubor(v pripade zadani argumentu --output) nebo vypisuje 
 * vysledek na standartni vystup(STDOUT)
 * 
 * @param neni
 * @return void
*/
function XMLwork(){
    require_once("global_variables.php");
    require_once("arguments.php");
    global $outputXML,$fromGlobal,$errors,$outputXMLName,$inputXMLName,$root,$literalStr;
    //Nezadan vstupni soubor
    if(!isset($inputXMLName)){
        error_exit($errors["NOT_INPUT"], 15);
    }
    
    if(isset($outputXMLName)){
        $outputXML = simplexml_load_file($outputXMLName);
    }else{
        if(isset($root)){
            $outputXML = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"."<".$root."/>");
        }else{
            $outputXML = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"UTF-8\"?><topKekichUtkin/>");
        }    
    }
   
    isXmlStructureValid($inputXMLName);
    $dom = new DOMDocument;
    
    $dom->load($inputXMLName);
    
    $s = simplexml_import_dom($dom);
  
    //Pokud literal nachazi mezi symboly " nebo ( a )
    literalStringTest($literalStr);
    
    //Vyhledavani v ROOT uzlu.Jinak vyhledavani v uzlu z fromGlobal
    if((isset($fromGlobal[0]))&&($fromGlobal[0] === "ROOT") && (!isset($fromGlobal[1]))){
           searchInRootNode($s);
    }else{ //Vyhledavani v elementu FROM
         //Pokud ROOT-element elementem z FROM
         if($s->getName() === $fromGlobal[0] && !isset($fromGlobal[1])){
             if(isset($fromGlobal[0]) && !isset($fromGlobal[1])){getElement($s);}
         //Pokud jmeno elementu neni ROOT, ale ROOT obsahuje steijny attribute jako attribute ze FROM    
         }elseif(($s->getName() === $fromGlobal[0]) && isset($fromGlobal[1])&& !empty($fromGlobal[0])){
                    $searched = FALSE;
                    foreach ($s->attributes() as $attributeName=>$attributeValue){
                        if($attributeName === $fromGlobal[1])
                        $searched = TRUE;
                    }
                    if(!$searched){searchInFromNode ($s);}
                    else{getElement($s);}            
             }elseif(empty($fromGlobal[0])&& isset ($fromGlobal[1])){
                 $searched = FALSE;
                 foreach ($s->attributes() as $attributeName=>$attributeValue){
                     if($attributeName === $fromGlobal[1]){$searched = TRUE;}
                 }if(!$searched){searchInFromNode($s);}else{getElement($s);}
         }else{searchInFromNode($s);}  //Pokud ROOT element neni element z FROM, hledame uzel, ze ktereho vybereme elementy ze SELECT
    }   
    
    outputFinal();
    
}
        

/*
 * Funkce pro vyhledavani elementu ze sekce FROM.Provadi vyhledavani do hloubky
 * Pokud soucasny uzel neni FROM element, rekurzivne volame s parametrem soucasneho uzlu
 * Pro vyhledavani elementu ze SELECT pouziva pomocnou funkci getElement(SimpleXMLElement );
 * 
 * @param SimpleXMLElement $node - root node ve kterem vyhledavame uzel ze FROM
 * @return void
*/
function searchInFromNode($node){
    global $fromGlobal,$counterForBreak,$fromCounter;
 
    //Vyhledavani v elementu(Nastaven pouze element)
    if(isset($fromGlobal[0]) && !isset($fromGlobal[1])){
        
        foreach ($node->children() as $a => $b){
            if($a != $fromGlobal[0]){
                searchInFromNode($b);
            }
            else{
                if($fromCounter > 0){break;}
                $fromCounter = $fromCounter + 1;
                getElement($b);                
            }
        }
    }else if((isset($fromGlobal[1]))&&(empty($fromGlobal[0]))){ //vyhledavani v attributu(Nastaven pouze attribute)

        foreach ($node->children() as $elementName=>$elementValue){
            if($counterForBreak == 1){break;}
            $searched = FALSE;
            foreach ($elementValue->attributes() as $attributeName=>$attributeValue){
                if($attributeName === $fromGlobal[1])
                    $searched = TRUE;
            }
            if(!$searched)
                searchInFromNode ($elementValue);
            else{
                if($fromCounter > 0){break;}
                $fromCounter = $fromCounter + 1;
                getElement($elementValue);               
            }            
        }    
    }else{ //vyhledavani v elementu s attributem(nastaven element a attribute)
       
        foreach ($node->children() as $elementName=>$elementValue){
            //priznak,ze element obsahuje attribute $fromGlobal[1]
            $searched = FALSE;
            foreach ($elementValue->attributes() as $attributeName=>$attributeValue){
                if($attributeName === $fromGlobal[1]){
                    $searched = TRUE;
                }
                    
            }
            if(($elementName != $fromGlobal[0]) ||(!$searched)){
                searchInFromNode ($elementValue);
            }
            else{
                if($fromCounter > 0){break;}
                $fromCounter = $fromCounter + 1;
                getElement ($elementValue);
            }
        }
    }
}


/*
 * Funkce provadi vyhledavani elementu <E> z uzlu FROM podle zadanych podminek WHERE
 * V pripade nalezeni elementu provadi zapis elementu do promenny SimpleXMLElement $outputXML
 * s vyuzitim funkce sxmlConvert(SimpleXMLElement , SimpleXMLElement)
 * 
 * @param SimplrXMLElement $node - uzel, ve kterem provadime vyhledavani
 * @return void
*/
function getElement($node){
    global $selectGlobal,$outputXML,$elemWhereGlobal,$limitGlobal,$counterLimit,$counterForBreak;

    if(isset($elemWhereGlobal[0]) && !isset($elemWhereGlobal[1])){ //Nataven pouze element (Sekce WHERE)
        foreach ($node->children() as $nodeName => $nodeValue){
            if($nodeName != $selectGlobal){
                getElement($nodeValue);
            }else{
                $elementTest = whereStatement($nodeValue);
                if($elementTest){
                    if(isset($limitGlobal)){
                        if($counterLimit < $limitGlobal){
                            $counterLimit = $counterLimit + 1;
                            sxmlConvert ($outputXML, $nodeValue);
                        }else{break;}
                    }else {sxmlConvert ($outputXML, $nodeValue);} 
                }
                else
                    continue;
            }
        }
    }elseif(isset($elemWhereGlobal[1])&& empty($elemWhereGlobal[0])){ // Nastaven pouze attribute (Sekce WHERE)
        foreach ($node->children() as $nodeName => $nodeValue){
            if(($nodeName != $selectGlobal)){
                getElement($nodeValue);
            }else{
                $searched = FALSE;
                foreach($nodeValue->attributes() as $attrName=>$attrValue){
                    //Element obsahuje attribute
                    if($attrName === $elemWhereGlobal[1]){
                        $searched = TRUE;
                        if(whereNoChild((String)$attrValue)){
                           if(isset($limitGlobal)){
                             if($counterLimit < $limitGlobal){//zpracovani LIMIT
                                $counterLimit = $counterLimit + 1;
                                sxmlConvert ($outputXML, $nodeValue);
                             }else{break;}
                            }else {sxmlConvert ($outputXML, $nodeValue);}
                        }
                        else{getElement($nodeValue);}
                        }if($searched == FALSE){getElement($nodeValue);}
                    }
                }
            }  
    }elseif(!empty($elemWhereGlobal[0]) && isset ($elemWhereGlobal[1])){ // Nastaven element a attribute (sekce WHERE)
            foreach ($node->children() as $nodeName => $nodeValue){
                if($nodeName != $selectGlobal){
                    getElement($nodeValue);
                }else{
                    if($nodeName === $elemWhereGlobal[0]){ //Pokud dotaz ve tvaru 'SELECT E WHERE E.A'
                        $searched = FALSE;
                        foreach ($nodeValue->attributes() as $attrName=>$attrValue){
                            if($attrName === $elemWhereGlobal[1]){ //Element <E> obshuje attribute z WHERE
                                $searched = TRUE;
                                //Pokud attribute splnuje podminky WHERE, je nutne zapsat element <E> do output
                                if(whereNoChild((String)$attrValue)){
                                    if(isset($limitGlobal)){ //LIMIT
                                        if($counterLimit < $limitGlobal){
                                            $counterLimit = $counterLimit + 1;
                                            sxmlConvert ($outputXML, $nodeValue);
                                        }else{break;}
                                    } else {
                                        sxmlConvert ($outputXML, $nodeValue);
                                    }   
                                }
                                //Jinak, element <E> ma attribute se stejnym nazvem, ktery nesplnuje podminku WHERE
                                else {getElement($nodeValue);}
                            }
                            //Element <E> ma stejny nazev, jako element <E> ze SELECT, ale nema attribute, podle ktereho provadime vyhledavani
                            if($searched == FALSE){getElement($nodeValue);}
                        }
                    }else{ //dotaz ve tvaru 'SELECT E WHERE C.A' - <C> musi byt synovsky uzel uvnitr uzlu <E>
                        foreach ($nodeValue->children() as $childName=>$childValue){//Vyhledavani synovskeho uzlu <C> uvnitr uzlu <E>
                            if($childName === $elemWhereGlobal[0]){ //Pokud element se rovna elementu <C> z WHERE
                                $searched = FALSE;
                                foreach ($childValue->attributes() as $attrName=>$attrValue){ //Vyhledavani attributu <A> v elem <C>
                                        if($attrName === $elemWhereGlobal[1]){
                                        $searched = TRUE;
                                        if(whereNoChild((String)$attrValue)){
                                            if(isset($limitGlobal)){
                                                if($counterLimit < $limitGlobal){ //LIMIT
                                                    $counterLimit = $counterLimit + 1;
                                                    sxmlConvert ($outputXML, $nodeValue);
                                                }else{break;}
                                            }else {sxmlConvert($outputXML, $nodeValue);}    
                                        }
                                        else {getElement($nodeValue);}//<C> ma attribute <A> ale s jinou hodnotou->hledame dalsi elem <E>
                                    }
                                    if($searched == FALSE){getElement($nodeValue);} //Element <C> nema attribute <A>->hledame dalsi elem <E>
                                }
                            }else{
                                continue;
                            }
                        }
                    }
                }
        } 
    }else{//Neni nastavena sekce WHERE, proste vyhledavani elementu <E> z uzlu
        foreach ($node->children() as $nodeName => $nodeValue){
            if($nodeName != $selectGlobal){    
                getElement($nodeValue);
            }else{
                if(isset($limitGlobal)){
                        if($counterLimit < $limitGlobal){
                            $counterLimit = $counterLimit + 1;
                            sxmlConvert ($outputXML, $nodeValue);
                        }else{break;}
                }else {sxmlConvert ($outputXML, $nodeValue);} 
            }
        }
    }
}


/*
 * Funkce hleda prvni vyskyt elementu z SELECT v ROOT node.Provadi vyhledavani do hloubky
 * Pokud soucasny uzel neni SELECT element, rekurzivne volame s parametrem soucasneho uzlu
 * 
 * @param SimpleXMLElement $node - root node ve kterem vyhledavame uzel ze SELECT
 * @return void
*/
function searchInRootNode($node){
    global $elemWhereGlobal,$limitGlobal,$counterLimit;
    global $outputXML,$selectGlobal;
    //Hledame podle elementu WHERE
    if(isset($elemWhereGlobal[0]) && !isset($elemWhereGlobal[1])){
        foreach ($node->children() as $nodeName => $nodeValue){
            //Pokud element nema synove, ale ma stejny nazev, jako <WHERE element> potrebujeme ten pridat do vystupniho souboru
            if($nodeValue->count() ==  0 && $nodeName === $elemWhereGlobal[0]){
                
                $elementTest = whereNoChild($nodeValue);//LIMIT 
                if($elementTest){
                    if(isset($limitGlobal)){
                        if($counterLimit < $limitGlobal){
                            $counterLimit = $counterLimit + 1;
                            sxmlConvert ($outputXML, $nodeValue);
                        }else{break;}
                    } else {sxmlConvert ($outputXML, $nodeValue);}         
                }
                else {continue;}                
            }
            //Pokud soucasny node neni <WHERE element> provadime rekurzivne volani funkce a vyhledavame do hloubky
            if($nodeName != $selectGlobal){
                searchInRootNode($nodeValue);
            }else{//Jinak, soucasny node z <WHERE element> sekce. 
                //Provadime kontrolu hodnoty elementu
                $elementTest = whereStatement($nodeValue);
                if($elementTest){ //LIMIT
                    if(isset($limitGlobal)){
                        if($counterLimit < $limitGlobal){
                            $counterLimit = $counterLimit + 1;
                            sxmlConvert ($outputXML, $nodeValue);
                        }else{break;}
                    } else {sxmlConvert ($outputXML, $nodeValue);} 
                }                    
                else
                    continue;
            }
        }        
    }elseif(isset($elemWhereGlobal[1]) && !empty($elemWhereGlobal[0])){//Provadi vyhledavani elementu s attributem, ktere jsou nastaveny ve WHERE
            foreach ($node->children() as $nodeName => $nodeValue){
                if($nodeName != $selectGlobal){
                    searchInRootNode($nodeValue);
                }else{
                    if($nodeName === $elemWhereGlobal[0]){ //Pokud dotaz ve tvaru 'SELECT E WHERE E.A'
                        $searched = FALSE;
                        foreach ($nodeValue->attributes() as $attrName=>$attrValue){
                            if($attrName === $elemWhereGlobal[1]){ //Element <E> obshuje attribute z WHERE
                                $searched = TRUE;
                                //Pokud attribute splnuje podminky WHERE, je nutne zapsat element <E> do output
                                if(whereNoChild((String)$attrValue)){
                                    if(isset($limitGlobal)){ //LIMIT
                                        if($counterLimit < $limitGlobal){
                                            $counterLimit = $counterLimit + 1;
                                            sxmlConvert ($outputXML, $nodeValue);
                                        }else{break;}
                                    } else {sxmlConvert ($outputXML, $nodeValue);}   
                                }
                                //Jinak, element <E> ma attribute se stejnym nazvem, ktery nesplnuje podminku WHERE
                                else {searchInRootNode($nodeValue);}
                            }
                            //Element <E> ma stejny nazev, jako element <E> ze SELECT, ale nema attribute, podle ktereho provadime vyhledavani
                            if($searched == FALSE){searchInRootNode($nodeValue);}
                        }
                    }else{ //dotaz ve tvaru 'SELECT E WHERE C.A' - <C> musi byt synovsky uzel uvnitr uzlu <E>
                        foreach ($nodeValue->children() as $childName=>$childValue){//Vyhledavani synovskeho uzlu <C> uvnitr uzlu <E>
                            if($childName === $elemWhereGlobal[0]){ //Pokud element se rovna elementu <C> z WHERE
                                $searched = FALSE;
                                foreach ($childValue->attributes() as $attrName=>$attrValue){ //Vyhledavani attributu <A> v elem <C>
                                        if($attrName === $elemWhereGlobal[1]){
                                        $searched = TRUE;
                                        if(whereNoChild((String)$attrValue)){
                                            if(isset($limitGlobal)){
                                                if($counterLimit < $limitGlobal){ //LIMIT
                                                    $counterLimit = $counterLimit + 1;
                                                    sxmlConvert ($outputXML, $nodeValue);
                                                }else{break;}
                                            } else {sxmlConvert ($outputXML, $nodeValue);}    
                                        }
                                        else {searchInRootNode($nodeValue);}//<C> ma attribute <A> ale s jinou hodnotou->hledame dalsi elem <E>
                                    }
                                    if($searched == FALSE){searchInRootNode($nodeValue);} //Element <C> nema attribute <A>->hledame dalsi elem <E>
                                }
                            }else{
                                continue;
                            }
                        }
                    }
                }
        }        
    }elseif(isset($elemWhereGlobal[1]) && empty($elemWhereGlobal[0])){//Provadi vyhledavani attributu.ktery nastaven ve <WHERE attribute>
        foreach ($node->children() as $nodeName => $nodeValue){
            if(($nodeName != $selectGlobal)){
                searchInRootNode($nodeValue);
            }else{
                $searched = FALSE;
                foreach($nodeValue->attributes() as $attrName=>$attrValue){
                    //Element obsahuje attribute
                    if($attrName === $elemWhereGlobal[1]){
                        $searched = TRUE;
                        if(whereNoChild((String)$attrValue)){
                           if(isset($limitGlobal)){
                             if($counterLimit < $limitGlobal){//LIMIT
                                $counterLimit = $counterLimit + 1;
                                sxmlConvert ($outputXML, $nodeValue);
                             }else{break;}
                            }else {sxmlConvert ($outputXML, $nodeValue);}
                        }
                        else{searchInRootNode($nodeValue);}
                        }if($searched == FALSE){searchInRootNode($nodeValue);}
                    }
                }
            }
    }else{ //Query neobsahuje sekce WHERE, hledame element <E> z <ROOT> (SELECT <E> FROM <ROOT>)
        foreach ($node->children() as $a => $b){
            if(($a != $selectGlobal)){
                searchInRootNode($b);
            }else{//LIMIT 
                if(isset($limitGlobal)){
                        if($counterLimit < $limitGlobal){
                            $counterLimit = $counterLimit + 1;
                            sxmlConvert ($outputXML, $b);
                        }else{break;} 
                } else {sxmlConvert ($outputXML, $b);} 
            }
        }
    }
}


/*
 * Funkce provadi covertace ze SimpleXMLElement do DOMElement
 * 
 * @param SimpleXMLElement $to - element, do ktereho ukladame (output file nebo promena)
 * @param SimpleXMLElement $from - element, ktery potrebujeme ulozit do vystupniho souboru nebo promenne
 *
 *  @returns void
 */
function sxmlConvert(SimpleXMLElement $to, SimpleXMLElement $from) {
    $toDom = dom_import_simplexml($to);
    $fromDom = dom_import_simplexml($from);
    $toDom->appendChild($toDom->ownerDocument->importNode($fromDom, true));
}


/*
 * Funkce provadi vyhodnoceni podminky v sekce WHERE 
 * 
 * @param SimpleXMLElement $node - uzel, ktery obsahuje hodnotu / hodnotu attributu z sekce WHERE
 * 
 * @returns TRUE - v pripade,ze uzel / attribute splnuje podminku WHERE
 * @returns FALSE - pokud uzel / attribute podminku nesplnuje
 */
function  whereStatement($node){
     global $elemWhereGlobal,$relationOp,$literalNum,$literalStr;
     $searched = FALSE;
     
     foreach ($node->children() as $elementName=>$elementValue){
         if($elementName === $elemWhereGlobal[0]){
             
             if($relationOp === "<"){
                    $literalNum = floatval($literalNum);
                    $tmpNum = floatval($elementValue);                    
                    if($literalNum > $tmpNum){return ($searched = TRUE);}                        
                    else{return ($searched = FALSE);}
                        
             }elseif($relationOp === ">"){
                    $literalNum = floatval($literalNum);
                    $tmpNum = floatval($elementValue);                    
                    if($literalNum < $tmpNum){return ($searched = TRUE);}        
                    else{return ($searched = FALSE);}
                        
             }elseif($relationOp === "="){
                    if(!isset($literalStr)){
                        $literalNum = floatval($literalNum);
                        $tmpNum = floatval($elementValue);                    
                        if($literalNum == $tmpNum){return ($searched = TRUE);}                        
                        else{return ($searched = FALSE);}
                    }else{
                        if($elementValue === $literalStr){return TRUE;}
                        else{return FALSE;}
                    }
                    
             }elseif($relationOp === "!="){
                    $literalNum = floatval($literalNum);
                    $tmpNum = floatval($elementValue);                    
                    if($literalNum != $tmpNum){return ($searched = TRUE);}    
                    else{return ($searched = FALSE);} 
                    
             }elseif($relationOp === "CONTAINS"){
                 if(substr_count($elementValue, $literalStr) > 0){return TRUE;}
                 else{return FALSE;}
                 
             }elseif($relationOp === "NOT CONTAINS"){
                 if(substr_count($elementValue, $literalStr) == 0){return TRUE;}
                 else{return FALSE;}
             }                       
        }         
    }
}


/*
 * Funkce provadi vyhodnoceni podminky v sekce WHERE 
 * 
 * @param SimpleXMLElement $elementValue - uzel, ktery obsahuje hodnotu / hodnotu attributu z sekce WHERE
 * 
 * @returns TRUE - v pripade,ze uzel / attribute splnuje podminku WHERE
 * @returns FALSE - pokud uzel / attribute podminku nesplnuje
 */
function whereNoChild($elementValue){
     global $relationOp,$literalNum,$literalStr;
     
       if($relationOp === "<"){
                    $literalNum = floatval($literalNum);
                    $tmpNum = floatval($elementValue);                    
                    if($literalNum > $tmpNum){return ($searched = TRUE);}                        
                    else{return ($searched = FALSE);}
                        
        }elseif($relationOp === ">"){
                    $literalNum = floatval($literalNum);
                    $tmpNum = floatval($elementValue);                    
                    if($literalNum < $tmpNum){return ($searched = TRUE);}        
                    else{return ($searched = FALSE);}
                        
        }elseif($relationOp === "="){
                if(!isset($literalStr)){
                    $literalNum = floatval($literalNum);
                    $tmpNum = floatval($elementValue);                    
                    if($literalNum == $tmpNum){return ($searched = TRUE);}                        
                    else{return ($searched = FALSE);}  
                }else{
                    if($elementValue === $literalStr){return TRUE;}
                    else{return FALSE;}
                }
                    
        }elseif($relationOp === "!="){
                    $literalNum = floatval($literalNum);
                    $tmpNum = floatval($elementValue);                    
                    if($literalNum != $tmpNum){return ($searched = TRUE);}    
                    else{return ($searched = FALSE);} 
                    
        }elseif($relationOp === "CONTAINS"){
            
                 if(substr_count($elementValue, $literalStr) > 0){return TRUE;}
                 else{return FALSE;}
                 
        }elseif($relationOp === "NOT CONTAINS"){
                 if(substr_count($elementValue, $literalStr) == 0){return TRUE;}
                 else{return FALSE;}
        }
}


/*
 * Funkce spracuje String literal z WHERE. Pokud literal mezi symboly " nebo (), je nutne odstranit
 * 
 * @param String $literal - "string nebo (string)
 * 
 * @returns void
 */
function literalStringTest($literal){
        global $literalStr;
        
        if(substr($literal,0,1) === "\"" && substr($literal, count($literal)-1,1) === "\""){
            $literalStr = substr($literalStr,1,count($literal)-2);
        }
        
        if(substr($literal,0,1) === "(" && substr($literal, count($literal)-2,1) === ")"){
            $literalStr = substr($literalStr,1,count($literal)-2);
        }
}


/*
 * Funkce zpracovava vstupni soubor. V pripade ze XML soubor obsahuje nejake chyby, ukonci program s navratovym kodem c.4 
 * 
 * @param String $file - vstupni XML soubor
 * @returns void
 */  
function isXmlStructureValid($file) {
    global $errors;
    
    $prev = libxml_use_internal_errors(true);
    $ret = true;
    try {
      new SimpleXMLElement($file, 0, true);
    } catch(Exception $e) {
      $ret = false;
    }
    if(count(libxml_get_errors()) > 0) {
      // Format vstupniho souboru neni validny
      outputFinal();
      error_exit($errors["WRONG_FILE_FORMAT"], 4);
      $ret = false;
    }
    libxml_clear_errors();
    libxml_use_internal_errors($prev);
  }


/*
 * Funkce zpracovava vystupni soubor : 
 * 1)V pripade ze nastaven parametr --root - obaluje vysledek 
 * 2)V pripade ze byl nastaven prepinac -n - odstrani XML hlavicku
 * 
 * @returns void
 */  
  function outputFinal(){
      global $root, $outputXMLName, $outputXML, $n;
      
      if(isset($outputXMLName)){
        $outputXML->asXML($outputXMLName);
   
        //-n parametr
        if($n){
            $list = file($outputXMLName);
            unset ($list[0]);
            file_put_contents($outputXMLName, implode('', $list));
        }
    
        if(!isset($root)){
            $list = file($outputXMLName);

            //Vysledny Soubor obsahuje hlavicku    
            if(!$n){
                if(trim($list[1])==="<topkekcheburek/>"){ //Vysledny XML soubor musi obsahovat pouze hlavicku
                    $list[1] = str_replace("<topkekcheburek/>", "", $list[1]); 
                    file_put_contents($outputXMLName, $list);    
                }else{ //vysledny soubor musi mit obsah a hlavicku
                    $list[1] = str_replace("<topkekcheburek>", "", $list[1]); 
                    $list[count($list)-1] = str_replace("</topkekcheburek>", "", $list[count($list)-1]);
                    file_put_contents($outputXMLName, $list);
                }
            }else{//Vysledny soubor neobsahuje hlavicku a musi byt prazny
                if(trim($list[0])==="<topkekcheburek/>"){//Vysledny soubor musi byt uplne prazny
                     $list[0] = str_replace("<topkekcheburek/>", "", $list[0]); 
                    file_put_contents($outputXMLName, $list);  
                }else{ // Vysledny soubor neobsahuje hlavicku,ale ma nejaky obsah 
                    $list[0] = str_replace("<topkekcheburek>", "", $list[0]); 
                    $list[count($list)-1] = str_replace("</topkekcheburek>", "", $list[count($list)-1]);
                    file_put_contents($outputXMLName, $list);
                }
            }
        } 
    }else{ //Vystup do STDOUT
        $finalOutput = $outputXML->asXML();
        $parse_output = explode("\n", $finalOutput);
        
        if($n){unset ($parse_output[0]);} 
        
        if(!$n){ //Vysledny XML soubor musi obshovat hlavicku, ale musi byt prazdny
                if(trim($parse_output[1])==="<topKekichUtkin/>"){$parse_output[1] = str_replace("<topKekichUtkin/>", "", $parse_output[1]);    
                }else{ //vysledny soubor musi mit obsah a hlavicku
                    $parse_output[1] = str_replace("<topKekichUtkin>", "", $parse_output[1]); 
                    $parse_output[count($parse_output)-2] = str_replace("</topKekichUtkin>", "", $parse_output[count($parse_output)-2]);
                    }
            }else{//Vysledny soubor neobsahuje hlavicku a musi byt prazny
                if(trim($parse_output[1])==="<topKekichUtkin/>"){$parse_output[1] = str_replace("<topKekichUtkin/>", "", $parse_output[1]); 
                }else{ // Vysledny soubor neobsahuje hlavicku,ale ma nejaky obsah 
                    $parse_output[1] = str_replace("<topKekichUtkin>", "", $parse_output[1]); 
                    $parse_output[count($parse_output)-1] = str_replace("</topKekichUtkin>", "", $parse_output[count($parse_output)-1]);
                }
            }
            fwrite(STDOUT, implode("\n", $parse_output));
    }   
      
  }

?>

